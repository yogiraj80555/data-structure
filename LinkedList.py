class Node:
	def __init__(self,value,next_node):
		self.value = value
		self.next = next_node; #we an directly pass None here, Choice may differ.....

class LinkedList:
	def __init__(self, value):
		node = Node(value, None);
		self.head = node
		self.tail = node
		self.length = 1;




	def append(self, value):
		#TODO: create new node append to end
		node = Node(value,None);
		if self.head is None:  # Might head is null
			self.__init__(value);
		else:
			self.tail.next = node
			self.tail = node
			self.length += 1




	def prepend(self, value):
		#TODO: create node and add to beginning
		node = Node(value,self.head);
		self.head = node;
		self.length +=1




	def pop(self):
		#TODO: Remove node form end node.
		if self.head is None:
			print("There is nothing to pop out");
			return

		current = self.head
		previous = None
		while current.next is not None:
			previous = current;
			current = current.next

		self.tail = previous
		if self.tail == None:
			self.head = self.tail
			return
		
		self.tail.next = None
		self.length -=1
		return current




	def pop_first(self):
		#TODO: remove node from begining

		# if linked list is empty
		if self.head == None:
			print("Linked list is empty")
			return
		temp = self.head;
		self.head = self.head.next;
		self.length -=1
		if self.head == None:
			self.tail = self.head
		return temp;




	def get(self,index:int):
		if self.length-1 < index or index < 0 or self.head == None:
			print("node not avalible");
			return
		temp = self.head;
		while index != 0:
			temp = temp.next;
			index -= 1
		print("Value is: ",temp.value)
		return temp





	def set(self,index:int,value:int):
		if self.length-1 < index or index < 0 or self.head == None:
			print("node not avalible");
			return
		temp = self.head;
		while index != 0:
			temp = temp.next;
			index -= 1
		temp.value = value;
		return temp





	def insert(self, index:int, value:int):
		#TODO: create node insert into position
		if self.length-1 < index or index < 0:
			print("node not avalible");
			return
		current = Node(value,None)
		temp = self.head
		prev = None
		while index != 0:
			index -= 1
			prev = temp
			temp = temp.next

		current.next = temp

		if prev == None: #for 0th index
			self.head = current;
		else:	
			prev.next = current

		self.length +=1

		return True




	def insert2(self, index:int, value:int):
		#TODO: create node insert into position
		if self.length < index or index < 0:
			print("node not avalible");
			return
		if index == 0:
			return self.prepend(value)
		if index == self.length:
			return self.append(value)

		node = Node(value,None);
		temp = self.get(index-1);
		node.next = temp.next;
		temp.next = node

		self.length +=1
		return True





	def remove(self,index:int):
		#TODO: remove node from index
		if self.length < index or index < 0:
			print("node not avalible");
			return
		elif index == 0:
			self.head = self.head.next
			return
		temp = self.get(index-1);
		remove = temp.next
		temp.next = temp.next.next
		self.length -= 1
		return remove



	def reverse(self,node):
		if node == None:
			return
		self.reverse(node.next)
		print(node.value,end=" ")




	def print(self):
		temp = self.head;
		while temp is not None:
			print(temp.value, end=",")
			temp = temp.next

		print("\nHead is: ",self.head,"    Tail is:",self.tail);



if __name__ == "__main__":
	choice = 9
	ask_index = "Enter Index: "
	ask_val = "Enter Value: "
	linked_list = LinkedList(4);

	def ask_choice():
		return int(input("\n\nEnter Choice: "))

	def ask_value(str):
		return input("\n"+str)

	def print_message():
		print('''\n\nExit press 0\n1 append\n2 prepend\n3 Pop Last
4 Pop First\n5 Get Element\n6 Set Element\n7 Insert\n8 remove\n9 reverse\n10 Print''')
	

	while choice != 0:
		print_message()
		choice = ask_choice()
		if choice == 1:
			linked_list.append(ask_value(ask_val))
		elif choice == 2:
			linked_list.prepend(ask_value(ask_val))
		elif choice == 3:
			linked_list.pop()
		elif choice == 4:
			linked_list.pop_first()
		elif choice == 5:
			linked_list.get(int(ask_value(ask_index)))
		elif choice == 6:
			linked_list.set(int(ask_value(ask_index)),int(ask_value(ask_val)))
		elif choice == 7:
			linked_list.insert2(int(ask_value(ask_index)),int(ask_value(ask_val)))
		elif choice == 8:
			linked_list.remove(int(ask_value(ask_index)))
		elif choice == 9:
			linked_list.reverse(linked_list.head)	
		elif choice == 10:
			print("Current LinkedList:")
			linked_list.print()

	print(linked_list.tail.next,"  ",linked_list.length);