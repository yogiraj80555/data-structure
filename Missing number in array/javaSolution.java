

class Solution {
    int missingNumber(int array[], int n) {
        Set<Integer> set = new HashSet<>();
        
        for (int i=0; i<n-1; i++) {
            int val = array[i];
            //adding into set
            set.add(val);
        }
        int min = 1;
        for (int i=0; i<n; i++){
            if (!set.contains(min)){
                return min;
            }
            min++;
        }
        return 0;
    }
}