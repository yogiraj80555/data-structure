class Solution:
    def missingNumber(self,array,n):
        hashSet = set()
        for i in range(n-1):
            hashSet.add(array[i])
       
        for i in range(n):
            if i+1 not in hashSet:
                return i+1
                
        return 0;